not' "Ivan"="nothing"
not' "Alex"="nothing"
not' "Lyova"="nothing"

dir' x = 
	if (x=="Ivan") then "Bio" else
	if (x=="Alex") then "PP" else
	if (x=="Lyova") then "Hell" else
	"nothing"
main = do
	x <- getLine
	putStrLn(dir'' x)

dir'' "Ivan" = "Bio"
dir'' "Alex" = "PP"
dir'' "Lyova" = "Hell"
dir'' x = "nothing"
--dir'' (not' "Ivan" || not' "Alex" || not' "Lyova") = "nothing"